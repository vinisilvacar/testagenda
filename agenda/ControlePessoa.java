package agenda;

import java.util.ArrayList;
import agenda.Pessoa;

public class ControlePessoa {

// Atributos

    private ArrayList<Pessoa> listaPessoas;

// Construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// Métodos
   
    //Adicionar pessoa
    public String adicionar(Pessoa umaPessoa) {
        String mensagem = "Pessoa adicionada com Sucesso!";
	listaPessoas.add(umaPessoa);
	return mensagem;
    }

    //Remover pessoa
    public String remover(Pessoa umaPessoa) {
        String mensagem1 = "Pessoa removida com sucesso";
        listaPessoas.remove(umaPessoa);
        return mensagem1;
    }
    
    //Pesquisar pessoa por nome
    public Pessoa pesquisarNome(String umNome) {
        for (Pessoa umaPessoa: listaPessoas) {
            if (pessoa.getNome().equalsIgnoreCase(umNome)) 
		return umaPessoa;
        }
        return null;
    }
   
    //Pesquisar pessoa por telefone
    public Pessoa pesquisarTelefone(String umTelefone) {
	for (Pessoa umaPessoa: listaPessoas) {
	     if (pessoa.getTelefone().equalsIgnoreCase(umTelefone))
		return umaPessoa;
	}	
     }
	
}
