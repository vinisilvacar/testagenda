package Test;

import static org.junit.Assert.*;
import agenda.Pessoa;


import org.junit.Before;
import org.junit.Test;

public class PessoaTest{
	Pessoa umaPessoa;

	@Before
	public void setUp() throws Exception {
		umaPessoa = new Pessoa();
	}

	@Test //Teste Nome
	public void testNome() {
		umaPessoa.setNome("Nome");
		assertEquals("Nome", umaPessoa.getNome());
		
	}
	
	@Test //Teste Idade
	public void testIdade() {
		umaPessoa.setIdade(18);
		assertEquals(18, umaPessoa.getIdade());
	}
		
	@Test //Teste Telefone
	public void testTelefone() {
		umaPessoa.setTelefone("3388-0055");
		assertEquals("3388-0055", umaPessoa.getTelefone());
	}

	@Test //Teste Sexo
	public void testSexo() {
		umaPessoa.setSexo('M');
		assertEquals('M', umaPessoa.getSexo());
	}
	
	@Test //Teste Email
	public void testEmail(){
		umaPessoa.setEmail("test@email.com");
		assertEquals("test@email.com", umaPessoa.getEmail());
	}
	
	@Test //Teste Hangout
	public void testHangout(){
		umaPessoa.setHangout("TestHangout");
		assertEquals("TestHangout", umaPessoa.getHangout());
	}
	
	@Test //Teste Endereço
	public void testEndereco(){
		umaPessoa.setEndereco("Qd.01 Casa 00 Brasília");
		assertEquals("Qd.01 Casa 00 Brasília", umaPessoa.getEndereco());
	}
	
	@Test //Teste RG
	public void testRg(){
		umaPessoa.setRg("1.234.567");
		assertEquals("1.234.567", umaPessoa.getRg());
	}
	
	@Test //Teste CPF
	public void testCpf(){
		umaPessoa.setCpf("123.456.789-00");
		assertEquals("123.456.789-00", umaPessoa.getCpf());
	}
}
