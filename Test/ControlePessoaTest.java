package Test;

import static org.junit.Assert.*;
import agenda.ControlePessoa;
import agenda.Pessoa;

import org.junit.Before;
import org.junit.Test;

public class ControlePessoaTest {
	private ControlePessoa umControle;
	private Pessoa umaPessoa;

	@Before
	public void setUp() throws Exception {
		umControle = new ControlePessoa(); //Instanciando objetos
		umaPessoa = new Pessoa();
	}
		
	@Test //Teste no método Adicionar
	public void testAdicionar() {
		assertEquals("Pessoa adicionada com Sucesso!",umControle.adicionar(umaPessoa));
	}
	
	@Test //Teste no método remover
	public void testRemover() {
		assertEquals("Pessoa removida com sucesso",umControle.remover(umaPessoa));
	}

	@Test
	public void testPesquisarNome() {
		String umNome = "Vinicius";
		umaPessoa.setNome(umNome);
		umControle.adicionar(umaPessoa); 
		
		assertEquals(umaPessoa, umControle.pesquisarNome(umNome));
	}
	
	@Test
	public void testPesquisarTelefone() {
		String umTelefone = "3355-8822";
		umaPessoa.setTelefone(umTelefone);
		umControle.adicionar(umaPessoa);
		
		assertEquals(umaPessoa, umControle.pesquisarTelefone(umTelefone));
	}
}
